


function sendAlert(type, message) {
    let alertType = 'alert-' + type

    let htmlAlert = '<h1 class="alert ' + alertType + '"><p>' + message + '</p><BR></h1>';

    $(".alert-message").prepend(htmlAlert);
    $(".alert-message .alert").first().hide().fadeIn(200).delay(2000).fadeOut(1000, function () { $(this).remove(); });
}

var myfunction; //ici que l'algo dans le langage choisi est ajouté

$("#btnCodeOnDemand").click(function (event) {
    $.ajax({
       /* beforeSend : function(req) {
            req.setRequestHeader('Authorization', 'Bearer ' + Cookies.get('token'));
        },*/
        url: 'http://localhost:3000/api/cesar/codeOnDemand/' + $("#inputGroupSelect").val(), //url
        type: 'GET',
        headers: {
            Authorization: 'Bearer ' + Cookies.get('token')
        },
        success: function (data) {

            myfunction = data.func;
            $("#choiceLang").hide();
            $("form#cesar").show()
            sendAlert("success", "<strong>Le script a bien été chargé </strong>")
        },
        error: function (err) {
            console.log(err)
            sendAlert("danger", "<strong>" + err.responseJSON.message + "</strong>")

        }
    });
    event.preventDefault();
});

$("#run_cesar").click(function (event) {

    eval(myfunction);
    var clair = $("input#msg_coder").val();
    var min_key = $("input#min_key").val();
    var max_key = $("input#max_key").val();

    // Fetch form to apply custom Bootstrap validation
    var form = $("#cesar");

    if (form[0].checkValidity() === false) {
        event.preventDefault();
        event.stopPropagation()
    } else {
        var return_data;
        return_data = Cesar(clair, min_key, 25); //function de cesar

        if (return_data.resultat === true) {
            sendAlert("success", "<strong>Trouvé !</strong>, la clé de cryptage <strong>" + return_data.cle + "</strong> a fonctionné")
            $("textarea#msg_decoder").val(return_data.text);
            $('input#msg_dechiffre').val(return_data.dechiffre)
        } else {
            sendAlert("danger", "<strong>Dommage</strong>, aucune clé de cryptage n'a fonctionné")

            $("textarea#msg_decoder").val(return_data.text);
        }
    }
    form.addClass('was-validated');
});


$("#run_slug").click(function (event) {

    $.ajax({

        url: 'http://localhost:3000/api/cesar/validationSlug', //url
        type: 'GET',
        dataType: 'json',
        headers: {
            Authorization: 'Bearer ' + Cookies.get('token')
        },
        success: function (data) {
            console.log(data)
            sendAlert("success", "Le slug à trouver est <strong>" + data.message + "</strong>")
        },
        error: function (err) {
            sendAlert("danger", "<strong>Bizarre</strong>Ce message n'est pas censé arriver")
        }
    });
    event.preventDefault();
});

$("#run_batch").click(function (event) {
    $.ajax({
        url: 'http://localhost:3000/api/cesar/batch', //url
        type: 'GET',
        headers: {
            Authorization: 'Bearer ' + Cookies.get('token')
        },
        success: function (data) {
            $("#msg_coder").val(data.secretMessage);
            $("input#min_key").val(data.minKey);
            $("input#max_key").val(data.maxKey);
        },
        error: function (err) {
            sendAlert("danger", "<strong>" + err.responseJSON.message + "</strong>")
        }
    });
    event.preventDefault();
});

$("#lost_batch").click(function (event) {
    $.ajax({
        url: 'http://localhost:3000/api/cesar/lost', //url
        type: 'POST',
        data: JSON.stringify({ 'secretMessage': $("#msg_coder").val() }),
        contentType: 'application/json',
        dataType: 'json',
        headers: {
            Authorization: 'Bearer ' + Cookies.get('token')
        },
        success: function (data) {

            $("#msg_coder").val('');
            $("input#min_key").val('');
            $("input#max_key").val('');

            sendAlert("success", "<strong>Le batch a été enlevé avec succés</strong>")

            socket.emit('lost', JSON.parse(this.data).secretMessage);
        },
        error: function (err) {
            if (err.status == 404) {
                sendAlert("success", "<strong>Le batch n'existe déjà plus dans la base</strong>")
            }

        }
    });
    event.preventDefault();
});


$("#found_batch").click(function (event) {
    $.ajax({
        url: 'http://localhost:3000/api/cesar/found', //url
        type: 'POST',
        data: JSON.stringify({ "secretMessage": $("#msg_coder").val(), "decalage": $("#validate_key").val(), "messageDechiffre": $("#msg_dechiffre").val() }),
        contentType: 'application/json',
        dataType: 'json',
        headers: {
            Authorization: 'Bearer ' + Cookies.get('token')
        },
        success: function (data) {
            $("#msg_coder").val('');
            $("input#min_key").val('');
            $("input#max_key").val('');
            $("input#validate_key").val('');

            sendAlert("success", "<strong>" + data.message + "</strong>")

            socket.emit('found', JSON.parse(this.data).secretMessage);

        },
        error: function (err) {
            if (err.status == 404 || err.status == 400) {
                sendAlert("danger", "<strong>" + err.responseJSON.message + "</strong>")
            }

        }
    });
    event.preventDefault();
});


$("#deconnect_btn").click(function (event) {
    Cookies.remove('token');
    window.location.replace("index.html");
});
