const jwt = require("jsonwebtoken");
const config = require("config");

module.exports = {

  /* FONCTIONNEL */
  /* authentification vérifie qu'un token jwt existe dans le header 
   Si le token existe, le middleware vérifie ce dernier , et le vérifie
   si tout est bon, il passe à un autre middleware soit à la route associée) */
  authentification: async function (req, res, next) {
    console.log(req)
    console.log(res)
    let possibleTokenHead = ['Bearer']
    
    var token = req.headers.authorization 

    if (!token) return res.status(403).send({ message: "Acces refusé, pas de token" });

    var tokenInformations = token.split(' ')

    if (!possibleTokenHead.includes(tokenInformations[0]))
      return res.status(403).send("Token incorrect");

    try {

      /* Code de base pour vérifier le token reçu avec la clé publique du serveur de CA   */
      /*
            let result = await axios.get('http://localhost:3000/apiexterne/get_public_key');
      
            if (result.status == '200') {
              const decoded = jwt.verify(tokenInformations[1], result.cle, function (err, decoded) {
                if (err) {
                  return res.status(403).send({ message: 'Token invalide' });
                } else {
                  next();
                }
              });
            }
      
            else {
              res.status(400).send({
                message: "API d'authentification : " + result.message
              })
            }
      */
     
     /* Code pour passer cette étape, ici le token demandé au client sera tokenOK */
      if (tokenInformations[1] == "tokenOK")
        next();
      else {
        return res.status(400).send({
          message: "UNE ERREUR DE MERDE"
        })
      }
    } catch (err) {
      res.status(400).send({ message: err.data });
    }
  }
}