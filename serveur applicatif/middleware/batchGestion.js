const bunker = require("../models/bunker")
const cesar = require("../utils/algo")
module.exports = {

  /* FONCTIONNEL */
  /* batchCreation renvoie un ensemble de données :
    secretMessage: message codé remis au hasard à partir du tableau bunker.secretMessages
    minKey et maxKey (ici par défauts toujours de 1 à 25)

    Si aucun batch n'est disponible, retourne un status 404
     */
  batchCreation: async function (req, res, next) {

    if(bunker.secretMessages.length < 1)
      return res.status(404).send({ message: "Aucun batch n'est disponible" })


    let randNb = Math.floor(Math.random() * bunker.secretMessages.length);
    let randomMessage = bunker.secretMessages[randNb];

    req.batch = { secretMessage: randomMessage, minKey: 1, maxKey: 25 };
    next();
  },

  /* FONCTIONNEL */
  /* batchVerification vérifie à l'aide de l'lago du serveur si (le message chiffré avec la clé) envoyés dans la requête
    correspondent au message déchiffré envoyé lui aussi dans la requête
    Si c'est en accord, le status 200 est envoyé sinon le status 400
     */
  batchVerification: async function (req, res, next) {


    if (req.body.secretMessage == "" || req.body.decalage == "" || req.body.messageDechiffre == "") {
      return res.status(404).send({ message: "Les données ne sont pas correctement entrées" })
    }


    let test = cesar(req.body.secretMessage, req.body.decalage)

    if (test != req.body.messageDechiffre.toUpperCase())
      return res.status(400).send({ message: "Le message n'est pas bien déchiffré" })


    next();
  },



}