const config = require("config");
const express = require("express");
var cors = require('cors');
const app = express();
const CesarRoute = require("./routes/cesar.route");



/*
  Utilisation du CORS pour configurer les autorisations du serveur
  Ici seules les headers authorization (pour le Bearer token) et content-type
  et l'origine du client doit être localhost 
*/
app.use(cors({
  allowedHeaders: ['authorization', 'content-type'],
  'origin': 'http://localhost',
  'methods': 'GET,HEAD,PUT,PATCH,POST,DELETE'
}));

//utilisation de json pour les réponses
app.use(express.json());

app.use("/api/cesar", CesarRoute);

const port = 3000;

app.listen(port, () => console.log(`Listening on port ${port}...`));