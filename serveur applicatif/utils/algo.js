module.exports = function Cesar(chiffre, decalage) {
    Alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ';
    chiffre  = chiffre.toUpperCase();
    clair = "";
    k        = parseInt(26 - decalage);
    while (k < 0) {
        k += 26
    }
    while (k > 25) {
        k -= 26
    }
    
    for (var count = 0; count < chiffre.length; count++) {
        alpha = chiffre.charAt(count);
        if (alpha === " ") {
            clair += " "
        } else {
            idx = Alphabet.indexOf(alpha);
            if (idx > -1)     // ne (dé)chiffre que les 26 lettres majuscules
            {
                clair += Alphabet.charAt(idx + k);
            }
        }
    }
    return clair;
}