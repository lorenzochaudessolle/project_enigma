var http = require('http');
var fs = require('fs');
var app = require('express')();
var server = require('http').Server(app);


// Chargement de socket.io
var io = require('socket.io').listen(server);
var count = 0;
var $clientConnected = [];

io.on('connection', function (socket) {

  /* 
    à chaque nouveau client connecté, un event counter est envoyé pour prévenir le client du nombre de clients totaux connectés */
  count++;
  io.emit('counter', { count: count });
  $clientConnected.push(socket.client.id)
  console.log(socket.client.id + ' is connected');


  /* 
    L'event lost est envoyé par le client lorsqu'il supprime un batch,
     une pop up doit être alors visible par le reste des clients connectés
  */
  socket.on('lost', function (data) {

    console.log("voici les données : " + data);
    socket.broadcast.emit('popup', { element: data, type: 'delete' });

  })

  /* 
    L'event found est envoyé par le client lorsqu'il valide un batch,
     une pop up doit être alors visible par le reste des clients connectés
  */
  socket.on('found', function (data) {

    console.log("voici les données : " + data);
    socket.broadcast.emit('popup', { element: data, type: 'found' });

  })


  /* 
    Lorsqu'un client se déconnecte, le nombre de clients connectés est mis à jour pour ré envoyé au client 
  */
  socket.on('disconnect', function () {

    count--;
    io.emit('counter', { count: count });
    console.log("client is disconnected");

  });
});

server.listen(8080, () => console.log(`Listening on port 8080...`));