
const express = require("express");
const router = express.Router();
const auth = require("../middleware/auth").authentification;

const { batchCreation, batchVerification } = require("../middleware/batchGestion");
const bunker = require("../models/bunker")
const algos = require("../models/algos").algos

/* FONCTIONNEL */

/*
  route GET le client demande le langage qu'il souhaite au serveur,
  celui-ci vérifie qu'il est présent dans la liste des langages, si c'est le cas
  il renvoie l'algorithme avec un status 200 sinon une erreur avec le status 404
*/
router.get("/codeOnDemand/:langageKey", auth, async (req, res) => {
  let result = algos.find(x => x.language == req.params.langageKey)
  if (result == null) {
    return res.status(404).send({message: "Langage pas disponible"})
  }
  else{
  res.status(200).send({ func: result.func})
  }
});

/* FONCTIONNEL */
/*
  route GET le client demande le slug à trouver dans les messages chiffrés,
  il renvoie le slug avec un status 200
*/
router.get("/validationSlug", auth, async (req, res) => {

  res.status(200).send({ message: bunker.validationSlug})

});

/* FONCTIONNEL */
/*
  route GET le client demande un batch à trouver dans les messages chiffrés,
  il renvoie un batch encore disponible avec un status 200 ou il returne un status 404 dans le middleware
*/
router.get("/batch", auth, batchCreation, async (req, res) => {
  res.status(200).send(req.batch)

});


/* FONCTIONNEL */
/*
  route POST le client demande de supprimer un batch incorrect,
  si le batch est présent dans la liste il le supprime et envoie un status 200
  sinon il renvoie un status 404
*/
router.post("/lost", auth, async (req, res) => {

  if (bunker.secretMessages.includes(req.body.secretMessage)) {
    bunker.secretMessages.splice(bunker.secretMessages.indexOf(req.body.secretMessage), 1);

    res.status(200).send({
      message: "batch retiré de la liste avec succès"
    });
  }
  else {
    res.status(404).send({
      message: "batch plus disponible"
    });
  }
});


/* FONCTIONNEL */
/*
  route POST le client demande de valider un batch incorrect,
  le batch est d'abord testé par l'algorithme du serveur avec la clé envoyée dans le header
  Si le batch est bien validé, s'il est dans la liste, il est supprimé et un status 200 est envoyé
  sinon il renvoie un status 404
*/
router.post("/found", auth, batchVerification, async (req, res) => {

  if (bunker.secretMessages.includes(req.body.secretMessage)) {
    bunker.secretMessages.splice(bunker.secretMessages.indexOf(req.body.secretMessage), 1);
    res.status(200).send({
      message: "Message déchiffré avec succés"
    });
  }
  else {

    res.status(404).send({
      message: "Batch plus disponible"
    });
  }

});



module.exports = router;